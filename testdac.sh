./jungle.sh wallet create -n jungle --to-console
PW5KiFgDzF324tXkd7i1yToyXhFquYKgWF9to95pWei28i8nXqotZ
./jungle.sh create key --to-console
5JLvFKmEFgcejrgNju1zh2kDSTUERaZgDSsC5KVwYJtGtx32MGo
EOS5mvhu2BCgTZq3DQGUUR6zPoXTDZqJv2MUnR2NSef2k72XSy9VM
dacauth11111
dactoken1111
daccustodia1
dacholding11
dacmultisig1
./jungle.sh wallet unlock -n jungle
./jungle.sh wallet import -n jungle
./jungle.sh system buyram dactoken1111 dactoken1111 "30.000 EOS"
./jungle.sh system delegatebw dactoken1111 dactoken1111 "10.000 EOS" "50.000 EOS"
./jungle.sh system buyram dacmultisig1 dacmultisig1 "30.000 EOS"
./jungle.sh system delegatebw dacmultisig1 dacmultisig1 "10.000 EOS" "50.000 EOS"
./jungle.sh system buyram dacholding11 dacholding11 "30.000 EOS"
./jungle.sh system delegatebw dacholding11 dacholding11 "10.000 EOS" "50.000 EOS"
./jungle.sh system buyram daccustodia1 daccustodia1 "60.000 EOS"
./jungle.sh system delegatebw daccustodia1 daccustodia1 "10.000 EOS" "20.000 EOS"
./jungle.sh system buyram dacauth11111 dacauth11111 "30.000 EOS"
./jungle.sh system delegatebw dacauth11111 dacauth11111 "10.000 EOS" "50.000 EOS"

mkdir daccontracts
cd daccontracts
git clone https://github.com/eosdac/eosdactoken.git
git clone https://github.com/eosdac/daccustodian.git
git clone https://github.com/eosdac/dacmultisigs.git
./jungle.sh set contract dactoken1111 ~/DAC/daccontracts/eosdactoken/output/jungle/eosdactokens -p dactoken1111

cd ~/DAC/daccontracts/daccustodian
./output/jungle/compile.sh

cd ~/DAC
./jungle.sh set contract daccustodia1 ~/DAC/daccontracts/daccustodian/output/jungle/daccustodian -p daccustodia1

cd ~/DAC/daccontracts/dacmultisigs
./output/jungle/compile.sh 

cd ~/DAC
./jungle.sh set contract dacmultisig1 ~/DAC/daccontracts/dacmultisigs/output/jungle/dacmultisigs -p dacmultisig1

git clone git@github.com:lukestokes/constitution.git

git clone https://github.com/eosusd/constitution.git

cd constitution
sed -i -- 's/eosDAC/DAC/g' constitution.md
git diff
git add .
git commit -m "Using DAC as an example"
git push

md5sum constitution.md
6d0ac9bb2e10441244c22160a29c50c8  constitution.md
e971bcf02c1d6d04a0e33d67f4c7411efc3070fa

a2022da6e770e0b2c3e5d3a10a560dc9
50de655bf59656d317222dedeb3c417358a0ecd9

echo '["https://raw.githubusercontent.com/eosusd/constitution/e971bcf02c1d6d04a0e33d67f4c7411efc3070fa/constitution.md", "6d0ac9bb2e10441244c22160a29c50c8"]' > terms.json

~/DAC/jungle.sh push action dactoken1111 newmemterms ~/DAC/terms.json -p dactoken1111

https://jungle.bloks.io/account/dactoken1111

echo "[daccustodian]" > ~/DAC/token_config.json
~/DAC/jungle.sh push action dactoken1111 updateconfig ~/DAC/token_config.json -p dactoken1111

~/DAC/jungle.sh push action dactoken1111 create '["dactoken1111", "10000000000.0000 DAC", 0]' -p dactoken1111
~/DAC/jungle.sh push action dactoken1111 issue '["dactoken1111", "1000000000.0000 DAC", "Issue"]' -p dactoken1111

echo '[["35000.0000 DAC", 5, 12, 60, "dacauth11111", "dacholding11", "", 1, 15, 3, 10, 9, 7, 7776000, "50.0000 EOS"]]' > ~/DAC/dac_config.json

(lockupasset)
(maxvotes)
(numelected)
(periodlength)
(authaccount)
(tokenholder)
(serviceprovider)
(should_pay_via_service_provider)
(initial_vote_quorum_percent)
(vote_quorum_percent)
(auth_threshold_high)
(auth_threshold_mid)
(auth_threshold_low)
(lockup_release_time_delay)
(requested_pay_max)
     
~/DAC/jungle.sh push action daccustodia1 updateconfig ~/DAC/dac_config.json -p daccustodia1

git clone https://github.com/eosusd/eosdactoolkit.git
cd eosdactoolkit/eosdac-material/src
git checkout dev
grep -lr --exclude-dir=".git" -e "eosDAC" . | xargs sed -i '' -e 's/eosDAC/DAC/g'
git commit -a -m "Replacing eosDAC with DAC"
git push

git diff
git commit -a -m "Updating jungle config for our own needs."
git push

sudo npm install -g vue-cli
# yarn global add vue-cli # or @vue/cli @vue/cli-init
sudo npm install -g quasar-cli
# yarn global add quasar-cli

cd ~/DAC/eosdactoolkit/eosdac-material
yarn install
cd ..
#yarn global add vue-cli
# Node.js >= 8.9.0 is required.
#yarn global add quasar-cli
./deploy-eosdac-material.sh jungle dev true

mongod

git clone https://github.com/eosusd/Actionscraper-rpc.git
cd Actionscraper-rpc
git checkout dev
yarn install
cp watchers/config.jungle.js watchers/config.js
node ./watchers/watcher_custodian.js

git clone https://github.com/eosusd/memberclient-api.git
cd memberclient-api

cp config.example.json config.json
npm install express
npm install request
node api_endpoint.js

echo '{
    "threshold" : 1,
    "keys" : [],
    "accounts": [{"permission":{"actor":"dacauth11111", "permission":"active"}, "weight":1}],
    "waits": []
}' > resign.json

echo '{
    "threshold": 2,
    "keys": [],
    "accounts": [
        {"permission":{"actor":"dacauth11111", "permission":"med"}, "weight":2},
        {"permission":{"actor":"daccustodia1", "permission":"eosio.code"}, "weight":1}
    ],
    "waits": [{"wait_sec":3600, "weight":1}]
}' > daccustodian_transfer.json

echo '{
    "threshold": 1,
    "keys": [{"key":"EOS5mvhu2BCgTZq3DQGUUR6zPoXTDZqJv2MUnR2NSef2k72XSy9VM", "weight":1}],
    "accounts": [
        {"permission":{"actor":"daccustodia1", "permission":"eosio.code"}, "weight":1}
    ],
    "waits": []
}' > dacauthority_owner.json

echo '{
    "threshold": 1,
    "keys": [{"key":"EOS5mvhu2BCgTZq3DQGUUR6zPoXTDZqJv2MUnR2NSef2k72XSy9VM", "weight":1}],
    "accounts": [
        {"permission":{"actor":"dacauth11111", "permission":"high"}, "weight":1}
    ],
    "waits": []
}' > dacauthority_active.json


cd ~/DAC
# These have to be set now because they are required in daccustodian_transfer.json
# These permissions are set in new period to the custodians with each configured threshold
./jungle.sh set account permission dacauth11111 high EOS5mvhu2BCgTZq3DQGUUR6zPoXTDZqJv2MUnR2NSef2k72XSy9VM owner -p dacauth11111@owner
./jungle.sh set account permission dacauth11111 med EOS5mvhu2BCgTZq3DQGUUR6zPoXTDZqJv2MUnR2NSef2k72XSy9VM high -p dacauth11111@owner
./jungle.sh set account permission dacauth11111 low EOS5mvhu2BCgTZq3DQGUUR6zPoXTDZqJv2MUnR2NSef2k72XSy9VM med -p dacauth11111@owner
./jungle.sh set account permission dacauth11111 one EOS5mvhu2BCgTZq3DQGUUR6zPoXTDZqJv2MUnR2NSef2k72XSy9VM low -p dacauth11111@owner

# resign dactokens account to dacauthority@active
./jungle.sh set account permission dactoken1111 active ./resign.json owner -p dactoken1111@owner
./jungle.sh set account permission dactoken1111 owner ./resign.json '' -p dactoken1111@owner

# resign dacmultisigs account to dacauthority@active
./jungle.sh set account permission dacmultisig1 active ./resign.json owner -p dacmultisig1@owner
./jungle.sh set account permission dacmultisig1 owner ./resign.json '' -p dacmultisig1@owner

# resign dacowner account to dacauthority@active, must allow timelocked transfers
# from daccustodian@eosio.code
# daccustodian_transfer.json allows the contract to make transfers with a time delay, or
# dacauthority@med without a time delay.  dacowner must have permission in xfer to transfer tokens
./jungle.sh set account permission dacholding11 xfer ./daccustodian_transfer.json active -p dacholding11@owner
./jungle.sh set action permission dacholding11 eosio.token transfer xfer -p dacholding11@owner
# Resign eosdacthedac
./jungle.sh set account permission dacholding11 active ./resign.json owner -p dacholding11@owner
./jungle.sh set account permission dacholding11 owner ./resign.json '' -p dacholding11@owner

# Create xfer permission and give it permission to transfer TESTDAC tokens
./jungle.sh set account permission daccustodia1 xfer ./daccustodian_transfer.json active -p daccustodia1@owner
./jungle.sh set action permission daccustodia1 dactoken1111 transfer xfer -p daccustodia1@owner
# Resign daccustodia1
./jungle.sh set account permission daccustodia1 active ./resign.json owner -p daccustodia1@owner
./jungle.sh set account permission daccustodia1 owner ./resign.json '' -p daccustodia1@owner

# Allow high to call any action on daccustodia1
./jungle.sh set action permission dacauth11111 daccustodia1 '' high -p dacauth11111@owner
# These 2 actions require a medium permission
./jungle.sh set action permission dacauth11111 daccustodia1 firecust med -p dacauth11111@owner
./jungle.sh set action permission dacauth11111 daccustodia1 firecand med -p dacauth11111@owner
# Allow one to call the multisig actions
./jungle.sh set action permission dacauth11111 dacmultisig1 '' one -p dacauth11111@owner
# set dacauthority@owner to point to daccustodian@eosio.code
./jungle.sh set account permission dacauth11111 active ./dacauthority_active.json owner -p dacauth11111@owner
# Only run this once you are done making any code changes:
./jungle.sh set account permission dacauth11111 owner ./dacauthority_owner.json '' -p dacauth11111@owner


./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcusto1 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcusto2 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcusto3 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcusto4 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcusto5 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcust11 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcust12 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcust13 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcust14 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcust15 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcust21 "10000000.0000 DAC" "" -p dactoken1111
./jungle.sh transfer -c dactoken1111 dactoken1111 eosusdcust22 "10000000.0000 DAC" "" -p dactoken1111

./jungle.sh create key --to-console
5JtTVnygkiVhQmb6yMymJWgWENPRfDkhEDH38MNh1F1FCicnHTH
EOS85dGYuemDvYH8SD4CqBfNhFV8eNUFLwfCfW7VdFXCaiMAPRs3F


eosusdcusto1
eosusdcusto2
eosusdcusto3
eosusdcusto4
eosusdcusto5
eosusdcust11
eosusdcust12
eosusdcust13
eosusdcust14
eosusdcust15
eosusdcust21
eosusdcust22

./jungle.sh get account daccustodia1
./jungle.sh get account eosusdcust22
./jungle.sh get account dacauth11111
./jungle.sh system buyram eosusdcust22 eosusdcust22 "5.0000 EOS" -p eosusdcust22




./jungle.sh push action dactoken1111 memberreg '["eosusdcusto1", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcusto1
./jungle.sh transfer -c dactoken1111 eosusdcusto1 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcusto1
./jungle.sh push action daccustodia1 nominatecand '["eosusdcusto1", "2.0000 EOS"]' -p eosusdcusto1
./jungle.sh push action daccustodia1 votecust '["eosusdcusto1",["eosusdcusto1"]]' -p eosusdcusto1

./jungle.sh push action dactoken1111 memberreg '["eosusdcusto2", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcusto2
./jungle.sh transfer -c dactoken1111 eosusdcusto2 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcusto2
./jungle.sh push action daccustodia1 nominatecand '["eosusdcusto2", "2.0000 EOS"]' -p eosusdcusto2
./jungle.sh push action daccustodia1 votecust '["eosusdcusto2",["eosusdcusto2"]]' -p eosusdcusto2

./jungle.sh push action dactoken1111 memberreg '["eosusdcusto3", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcusto3
./jungle.sh transfer -c dactoken1111 eosusdcusto3 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcusto3
./jungle.sh push action daccustodia1 nominatecand '["eosusdcusto3", "2.0000 EOS"]' -p eosusdcusto3
./jungle.sh push action daccustodia1 votecust '["eosusdcusto3",["eosusdcusto3"]]' -p eosusdcusto3

./jungle.sh push action dactoken1111 memberreg '["eosusdcusto4", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcusto4
./jungle.sh transfer -c dactoken1111 eosusdcusto4 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcusto4
./jungle.sh push action daccustodia1 nominatecand '["eosusdcusto4", "2.0000 EOS"]' -p eosusdcusto4
./jungle.sh push action daccustodia1 votecust '["eosusdcusto4",["eosusdcusto4"]]' -p eosusdcusto4

./jungle.sh push action dactoken1111 memberreg '["eosusdcusto5", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcusto5
./jungle.sh transfer -c dactoken1111 eosusdcusto5 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcusto5
./jungle.sh push action daccustodia1 nominatecand '["eosusdcusto5", "2.0000 EOS"]' -p eosusdcusto5
./jungle.sh push action daccustodia1 votecust '["eosusdcusto5",["eosusdcusto5"]]' -p eosusdcusto5

./jungle.sh push action dactoken1111 memberreg '["eosusdcust11", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcust11
./jungle.sh transfer -c dactoken1111 eosusdcust11 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcust11
./jungle.sh push action daccustodia1 nominatecand '["eosusdcust11", "2.0000 EOS"]' -p eosusdcust11
./jungle.sh push action daccustodia1 votecust '["eosusdcust11",["eosusdcust11"]]' -p eosusdcust11

./jungle.sh push action dactoken1111 memberreg '["eosusdcust12", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcust12
./jungle.sh transfer -c dactoken1111 eosusdcust12 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcust12
./jungle.sh push action daccustodia1 nominatecand '["eosusdcust12", "2.0000 EOS"]' -p eosusdcust12
./jungle.sh push action daccustodia1 votecust '["eosusdcust12",["eosusdcust12"]]' -p eosusdcust12

./jungle.sh push action dactoken1111 memberreg '["eosusdcust13", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcust13
./jungle.sh transfer -c dactoken1111 eosusdcust13 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcust13
./jungle.sh push action daccustodia1 nominatecand '["eosusdcust13", "2.0000 EOS"]' -p eosusdcust13
./jungle.sh push action daccustodia1 votecust '["eosusdcust13",["eosusdcust13"]]' -p eosusdcust13

./jungle.sh push action dactoken1111 memberreg '["eosusdcust14", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcust14
./jungle.sh transfer -c dactoken1111 eosusdcust14 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcust14
./jungle.sh push action daccustodia1 nominatecand '["eosusdcust14", "2.0000 EOS"]' -p eosusdcust14
./jungle.sh push action daccustodia1 votecust '["eosusdcust14",["eosusdcust14"]]' -p eosusdcust14

./jungle.sh push action dactoken1111 memberreg '["eosusdcust15", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcust15
./jungle.sh transfer -c dactoken1111 eosusdcust15 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcust15
./jungle.sh push action daccustodia1 nominatecand '["eosusdcust15", "2.0000 EOS"]' -p eosusdcust15
./jungle.sh push action daccustodia1 votecust '["eosusdcust15",["eosusdcust15"]]' -p eosusdcust15

./jungle.sh push action dactoken1111 memberreg '["eosusdcust21", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcust21
./jungle.sh transfer -c dactoken1111 eosusdcust21 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcust21
./jungle.sh push action daccustodia1 nominatecand '["eosusdcust21", "2.0000 EOS"]' -p eosusdcust21
./jungle.sh push action daccustodia1 votecust '["eosusdcust21",["eosusdcust21"]]' -p eosusdcust21

./jungle.sh push action dactoken1111 memberreg '["eosusdcust22", "6d0ac9bb2e10441244c22160a29c50c8"]' -p eosusdcust22
./jungle.sh transfer -c dactoken1111 eosusdcust22 daccustodia1 "35000.0000 DAC" "daccustodia1" -p eosusdcust22
./jungle.sh push action daccustodia1 nominatecand '["eosusdcust22", "2.0000 EOS"]' -p eosusdcust22
./jungle.sh push action daccustodia1 votecust '["eosusdcust22",["eosusdcust22"]]' -p eosusdcust22




./jungle.sh push action daccustodia1 newperiod '{"message":"New Period"}' -p eosusdcust22